import React, {useState, useEffect} from 'react';
import	{Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';


export default function Register(){
	// store values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if((email !=='' && password1 !== '' && password2 !=='') && (password1 === password2)){
			setIsActive(true);
			let globalPass = password1;
			let globalEmail = email;

		} else{
			setIsActive(false);
		}
	}, [email, password1, password2])

	
	function registerUser(e){
		e.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');
		Swal.fire({
		  title: 'Good job!',
		  text: 'Registration successful!',
		  icon:'success'
		})
	}

	return (

		<Form className = "p-2" onSubmit={(e) => registerUser(e) }>
			<Form.Group>
				<h1 className = "text-center mb-2">Register</h1>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder = "ex.: juan@email.com"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className = "text-muted">
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder = "Enter password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
				<Form.Text className = "text-muted">
					User combination of lowercase and uppercase texts, special characters and numbers.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder = "Verify password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>
			</Form.Group>

			{isActive ?
				<Button className="mt-3" variant="primary" type="submit" > Submit </Button>
				:
				<Button className="mt-3" variant="primary" type="submit" disabled> Submit </Button>
			}

		</Form>
	)
}