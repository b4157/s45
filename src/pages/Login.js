import React, {useState, useEffect} from 'react';
import	{Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import globalPass from './Register.js'
import globalEmail from './Register.js'

export default function Login(){
	// store values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(email !==globalEmail && password1 !== globalPass){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password1])

	
	function registerUser(e){
		e.preventDefault();

		setEmail('');
		setPassword1('');
		Swal.fire({
		  title: 'Good job!',
		  text: 'Login successful!',
		  icon:'success'
		})
	}

	return (

	<Form className = "p-2" onSubmit={(e) => registerUser(e) }>
		<Form.Group>
			<h1 className = "text-center mb-2">Login</h1>
			<Form.Label>Email Address</Form.Label>
			<Form.Control 
				type="email"
				placeholder = "ex.: juan@email.com"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
			/>
		
		</Form.Group>

		<Form.Group>
			<Form.Label>Password</Form.Label>
			<Form.Control 
				type="password"
				placeholder = "Enter password"
				required
				value={password1}
				onChange={e => setPassword1(e.target.value)}
			/>
		
		</Form.Group>

		

		{isActive ?
			<Button className="mt-3" variant="primary" type="submit" > Login </Button>
			:
			<Button className="mt-3" variant="primary" type="submit" disabled> Login </Button>
		}

	</Form>
	)
}