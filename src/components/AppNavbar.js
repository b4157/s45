import React from 'react';
// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'

import {Navbar, Nav} from 'react-bootstrap';

export default function AppNavbar(){
	return (
			<Navbar bg="dark" expand="lg" variant="dark">
				<Navbar.Brand className="ms-2" href="#home">Zuitt Booking</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse  id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link href="#home">Home</Nav.Link>
						<Nav.Link href="#courses">Courses</Nav.Link>
						<Nav.Link href="#register">Register</Nav.Link>
						<Nav.Link href="#login">Login</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}