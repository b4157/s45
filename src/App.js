import './App.css';
import AppNavBar from'./components/AppNavbar';
import Home from'./pages/Home';
import Courses from'./pages/Courses';
import Register from './pages/Register'
import Login from './pages/Login'
import React from 'react';
import  {Container} from "react-bootstrap";

let globalPass = '';

function App() {

  return (
    
    <>
      <AppNavBar />
      
      <Container>
        <Home />
        <Courses />
        
        <Register/>
        <Login />
      </Container>
      
    </>

  );
}

export default App;
